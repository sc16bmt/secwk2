@myapp.route('/', methods=['GET', 'POST'])
def HomePage():
    return render_template("base.html")

def handle_elegantly(name):
    db.session.add(User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp))
    db.session.flush()
    print ('Exception elegantly handled!!')

@myapp.route('/Signup', methods=['GET', 'POST'])
def Signup():
    form = SignupForm()
    if form.validate_on_submit():
        for p in models.User.query.all():
            if form.Email.data == p.Email and form.UN.data == p.Username:
                return redirect(url_for("FSignupeu"))
            if form.Email.data == p.Email:
                return redirect(url_for("FSignupe"))
            if form.UN.data == p.Username:
                return redirect(url_for("FSignupu"))
        if request.method == 'POST':
            hp = generate_password_hash(form.pswds.data, method = 'sha256')
            new_user = User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp)
            db.session.add(new_user)
            db.session.commit()
            try:
                db.session.add(new_user)
                db.session.commit()
                db.session.flush()
            except exc.IntegrityError:
                db.session.rollback()
                handle_elegantly('This will run fine')
            flash('You registered succesfully. You can now login.')
            return redirect(url_for('HomePage'))
        else:
            flash ('Invalid Data entered. Please try again!')
            return redirect(url_for("FSignup"))
    else:
        if request.method == 'POST':
            flash ('Invalid Data entered. Please try again!')
    return render_template("signup.html", form = form)

@myapp.route('/FSignupe', methods=['GET', 'POST'])
def FSignupe():
    form = SignupForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            for p in models.User.query.all():
                if form.Email.data == p.Email and form.UN.data == p.Username:
                    return redirect(url_for("FSignupeu"))
                if form.Email.data == p.Email:
                    return redirect(url_for("FSignupe"))
                if form.UN.data == p.Username:
                    return redirect(url_for("FSignupu"))
            hp = generate_password_hash(form.pswds.data, method = 'sha256')
            new_user = User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp)
            db.session.add(new_user)
            db.session.commit()
            flash('You registered succesfully. You can now login.')
            return redirect(url_for('HomePage'))
        else:
            flash ('Invalid Data entered. Please try again!')
            return redirect(url_for("FSignupe"))
    else:
        if request.method == 'POST':
            flash ('Invalid Data entered. Please try again!')
    return render_template("fsignup.html", form = form, parameter = "Email")

@myapp.route('/FSignupu', methods=['GET', 'POST'])
def FSignupu():
    form = SignupForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            for p in models.User.query.all():
                if form.Email.data == p.Email and form.UN.data == p.Username:
                    return redirect(url_for("FSignupeu"))
                if form.Email.data == p.Email:
                    return redirect(url_for("FSignupe"))
                if form.UN.data == p.Username:
                    return redirect(url_for("FSignupu"))
            hp = generate_password_hash(form.pswds.data, method = 'sha256')
            new_user = User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp)
            db.session.add(new_user)
            db.session.commit()
            flash('You registered succesfully. You can now login.')
            return redirect(url_for('HomePage'))
        else:
            flash ('Invalid Data entered. Please try again!')
            return redirect(url_for("FSignupu"))
    else:
        if request.method == 'POST':
            flash ('Invalid Data entered. Please try again!')
    return render_template("fsignup.html", form = form, parameter = "Username")

@myapp.route('/FSignupeu', methods=['GET', 'POST'])
def FSignupeu():
    form = SignupForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            for p in models.User.query.all():
                if form.Email.data == p.Email and form.UN.data == p.Username:
                    return redirect(url_for("FSignupeu"))
                if form.Email.data == p.Email:
                    return redirect(url_for("FSignupe"))
                if form.UN.data == p.Username:
                    return redirect(url_for("FSignupu"))
            hp = generate_password_hash(form.pswds.data, method = 'sha256')
            new_user = User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp)
            db.session.add(new_user)
            db.session.commit()
            flash('You registered succesfully. You can now login.')
            return redirect(url_for('HomePage'))
        else:
            flash ('Invalid Data entered. Please try again!')
            return redirect(url_for("FSignupeu"))
    else:
        if request.method == 'POST':
            flash ('Invalid Data entered. Please try again!')
    return render_template("fsignup.html", form = form, parameter = "Email and Username")

@myapp.route('/Login', methods=['GET', 'POST'])
def Login():
    form = LoginForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            session.pop('user', None)
            userfr = User.query.filter_by(Username = form.UN.data).first()
            if userfr:
                if check_password_hash(userfr.pswd,form.pswd.data):
                    session['user'] =  userfr.Username
                    if 'user' in session:
                        login_user(userfr, remember = form.remember.data)
                    return redirect(url_for('HomePage'))
                else:
                    flash ('Invalid Password or username. Please try again!')
            else:
                flash ('Invalid Password or username. Please try again!')
        else:
            flash ('Invalid Password or username. Please try again!')
    else:
        if request.method == 'POST':
            flash ('Invalid Password or username. Please try again!')
    return render_template('login.html', form = form)
