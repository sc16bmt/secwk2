from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user


myapp = Flask(__name__)
myapp.config.from_object('config')
db = SQLAlchemy(myapp)
login_manager = LoginManager()
login_manager.init_app(myapp)
login_manager.login_view = "login"

migrate = Migrate(myapp, db)

from app import core, models
