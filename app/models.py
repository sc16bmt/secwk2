from app import db
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user, login_manager

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    First_name = db.Column(db.String(500), index=True)
    Last_name = db.Column(db.String(500), index=True)
    DOB = db.Column(db.DateTime)
    Email = db.Column(db.String(500), index=True, unique=True)
    Username = db.Column(db.String(50), index=True, unique=True)
    pswd = db.Column(db.String(500), index=True)

class Diary(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Username = db.Column(db.String(50), index=True)
    Date = db.Column(db.String(500), index=True)
    Time = db.Column(db.String(500), index=True)
    Input = db.Column(db.String(10,000), index=True)

class Drugs(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Drugname = db.Column(db.String(50), index=True)
    Username = db.Column(db.String(50), index=True)
    Times = db.Column(db.Integer, index=True)

class Mood(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    Mood = db.Column(db.String(50), index=True)
    Username = db.Column(db.String(50), index=True)
    Date = db.Column(db.String(500), index=True)
    Time = db.Column(db.String(500), index=True)
