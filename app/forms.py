from flask_wtf import Form
from wtforms import IntegerField, PasswordField, TextField, DateField, BooleanField, FloatField, SelectField, TextAreaField, RadioField
from wtforms.validators import DataRequired, EqualTo, Email, ValidationError,Length
from werkzeug.security import generate_password_hash, check_password_hash
from app import models
from .models import User

def check_un_error(form, field):
    for p in models.User.query.all():
        if field.data == p.Username:
            raise ValidationError('User with that name already exists.')

def check_mail_error(form, field):
    for p in models.User.query.all():
        if field.data == p.Email:
            raise ValidationError('User with that email already exists.')

def times(form, field):
    if field.data > 6:
        raise ValidationError('Too many times a day. Invalid number')

def isint(form, field):
    if not(isinstance(field.data, (int))):
        raise ValidationError('Not an integer')


class LoginForm(Form):
    UN = TextField('UN', validators=[DataRequired()])
    pswd = PasswordField("pswd", validators=[DataRequired()])
    remember = BooleanField('remember')

class SignupForm(Form):
    FN = TextField('FN', validators=[DataRequired()])
    LN = TextField('LN', validators=[DataRequired()])
    DOB = DateField('DOB', validators=[DataRequired()], format='%d-%m-%Y')
    Email = TextField('Email',validators=[DataRequired(), Email(), check_mail_error])
    UN = TextField('UNS', validators=[DataRequired(),check_un_error])
    pswds = PasswordField("pswds", validators=[DataRequired(), EqualTo("copswd", message = "Passwords must match")])
    copswd = PasswordField("copswd")

class DiaryForm(Form):
    Input = TextAreaField('FN', validators=[DataRequired()])

class DrugsForm(Form):
    Drugname = TextField('DN', validators=[DataRequired()])
    Times = IntegerField('Times a day', validators=[DataRequired(), times, isint])

class ProgressForm(Form):
    Mood = RadioField('Label', choices=[('1','Great'),('2','Good'),('3','Okay'),('4','Not too Good'),('5','Terrible')])
