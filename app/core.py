from app import myapp
from flask import Flask, render_template, url_for, redirect, logging, session, request, flash, g
from werkzeug.security import generate_password_hash, check_password_hash
from .forms import SignupForm, LoginForm, DiaryForm, DrugsForm, ProgressForm
from app import db, models
from .models import User, Diary, Drugs
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import exc
from sqlalchemy.orm import sessionmaker, scoped_session
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user, login_manager
import datetime

login_manager = LoginManager()
login_manager.init_app(myapp)
login_manager.login_view = "Login"

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


@myapp.route('/', methods=['GET', 'POST'])
def HomePage():
    return render_template("base.html")


@myapp.route('/Signup', methods=['GET', 'POST'])
def Signup():
    form = SignupForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            hp = generate_password_hash(form.pswds.data, method = 'sha256')
            new_user = User(First_name= form.FN.data,Last_name= form.LN.data, DOB = form.DOB.data, Email = form.Email.data, Username = form.UN.data, pswd = hp)
            db.session.add(new_user)
            db.session.commit()
            flash('You registered succesfully. You can now login.')
            return redirect(url_for('HomePage'))
    else:
        if request.method == 'POST':
            flash ('Invalid Data entered. Please try again!')
    return render_template("signup.html", form = form)

@myapp.route('/Login', methods=['GET', 'POST'])
def Login():
    form = LoginForm()
    if form.validate_on_submit():
        if request.method == 'POST':
            session.pop('user', None)
            userfr = User.query.filter_by(Username = form.UN.data).first()
            if userfr:
                if check_password_hash(userfr.pswd,form.pswd.data):
                    session['user'] =  userfr.Username
                    if 'user' in session:
                        login_user(userfr, remember = form.remember.data)
                    return redirect(url_for('dash'))
                else:
                    flash ('Invalid Password or username. Please try again!')
            else:
                flash ('Invalid Password or username. Please try again!')
        else:
            flash ('Invalid Password or username. Please try again!')
    else:
        if request.method == 'POST':
            flash ('Invalid Password or username. Please try again!')
    return render_template('login.html', form = form)

@myapp.before_request
def bef_req():
    g.user = None
    if 'user' in session:
        g.user = session['user']

@myapp.route('/getsesh', methods=['GET', 'POST'])
def get_session():
    if 'user' in session:
        return "Happy days"
    else:
        return "Nada"

@myapp.route('/dashboard', methods=['GET', 'POST'])
@login_required
def dash():
    if g.user:
        return render_template('dashboard.html', name = current_user.Username)
    else:
        return redirect(url_for('login'))

@myapp.route('/messages', methods=['GET', 'POST'])
@login_required
def messages():
    if g.user:
        return render_template('messages.html')
    else:
        return redirect(url_for('Login'))

@myapp.route('/progress', methods=['GET', 'POST'])
@login_required
def progress():
    if g.user:
        form = DrugsForm()
        c= 0;
        form2 = ProgressForm()

        now = datetime.date.today()
        day = datetime.date.weekday(now)
        if form.validate_on_submit() and form.submit.data:
            if request.method == 'POST':
                new_entry = Drugs(Drugname = form.Drugname.data, Username = current_user.Username, Times = form.Times.data)
                db.session.add(new_entry)
                db.session.commit()
                flash('You succesfully entered your medicine.')
                return render_template('progress.html', day = day, form = form, form2 = form2)
            return render_template('progress.html', day = day, form = form, form2 = form2)
            print("Done")
        return render_template('progress.html', day = day, form = form, form2 = form2)
        if form2.validate_on_submit() and form2.submit.data:
            print("Done")
            if request.method == 'POST':
                print("Done")
                bro = datetime.datetime.now()
                if form2.Mood.value == "Great":
                    new_entry = Mood(Mood = "Great", Username = current_user.Username, Date = bro.strftime("%d-%m-%Y"), Time = bro.strftime("%H:%M") )
                    db.session.add(new_entry)
                    db.session.commit()
                    c = 1
                    flash("Done")
                    return render_template('progress.html', day = day, form = form, form2 = form2, c = c)
                else:
                    c=0
                    flash("Exxxxxxxx")
                    return render_template('progress.html', day = day, form = form, form2 = form2, c = c)
        return render_template('progress.html', day = day, form = form, form2 = form2 , c=c)
    else:
        return redirect(url_for('Login'))

@myapp.route('/diary', methods=['GET', 'POST'])
@login_required
def diary():
    if g.user:
        form = DiaryForm()
        my_list = []
        c = 0
        for i in models.Diary.query.filter_by(Username = current_user.Username).all():
            my_list.append(c)
            c+= 1
        un = current_user.Username
        if form.validate_on_submit()   :
            if request.method == 'POST':
                now = datetime.datetime.now()
                new_entry = Diary(Username = current_user.Username, Date = now.strftime("%d-%m-%Y"), Time = now.strftime("%H:%M") , Input = form.Input.data)
                db.session.add(new_entry)
                db.session.commit()
                return redirect(url_for('diary'))
        return render_template('diary.html', form = form, UN = un, no = models.Diary, array = my_list, zip = zip)
    else:
        return redirect(url_for('Login'))

@myapp.route('/profile', methods=['GET', 'POST'])
@login_required
def profile():
    if g.user:
        return render_template('profile.html')
    else:
        return redirect(url_for('Login'))



@myapp.route('/logout')
@login_required
def logout():
    logout_user()
    session.pop('user', None)
    return redirect(url_for('HomePage'))
